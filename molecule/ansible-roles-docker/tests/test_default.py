def test_docker_is_installed(host):
    assert host.package("docker-ce").is_installed
